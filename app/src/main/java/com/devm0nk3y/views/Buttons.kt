package com.devm0nk3y.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Switch
import android.widget.Toast
import android.widget.ToggleButton
import androidx.core.content.ContextCompat
import androidx.core.view.get
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton

class Buttons : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buttons)

        var iniciarSesionButton = findViewById<Button>(R.id.iniciarSesionButton)
        iniciarSesionButton.setOnClickListener {
            Toast.makeText(this, "Boton pulsado", Toast.LENGTH_SHORT).show()
        }

        var imageButton = findViewById<ImageButton>(R.id.imageButton)
        imageButton.setOnClickListener {
            Toast.makeText(this, "ImageBoton pulsado", Toast.LENGTH_SHORT).show()
        }

        var condicionesChipGroup = findViewById<ChipGroup>(R.id.condicionesChipGroup)
        var chip: Chip


        for(index in 0..condicionesChipGroup.childCount-1) {
            chip = condicionesChipGroup.get(index) as Chip

            chip.setOnCloseIconClickListener {
                condicionesChipGroup.removeView(it)
            }

            chip.setOnClickListener {
                var aux = it as Chip
                Toast.makeText(this, "${aux.text} pulsado", Toast.LENGTH_SHORT).show()
            }

        }

        val chipNew = Chip(this)
        chipNew.text = "Opcion"
        chipNew.chipIcon = ContextCompat.getDrawable(this, R.drawable.icon_car)
        chipNew.isChipIconVisible = false
        chipNew.isCloseIconVisible = true
        chipNew.isClickable = true
        chipNew.isCheckable = false
        condicionesChipGroup.addView(chipNew as View)
        chipNew.setOnCloseIconClickListener {
            condicionesChipGroup.removeView(chipNew as View)
        }

        var vacacionesRadioGroup = findViewById<View>(R.id.vacacionesRadioGroup) as RadioGroup
        var rb = vacacionesRadioGroup.getChildAt(1) as RadioButton

        vacacionesRadioGroup.check(rb.id)

        var seguroCheckBox = findViewById<CheckBox>(R.id.seguroCheckBox)
        seguroCheckBox.isEnabled = true
        seguroCheckBox.isChecked = true

        var normalToggle: ToggleButton = findViewById(R.id.normalToggleButton)
        normalToggle.setOnCheckedChangeListener {_, isChecked ->

            if (isChecked) {
                Toast.makeText(this, "Toggle activado", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Toggle desactivado", Toast.LENGTH_SHORT).show()
            }

        }

        var normalSwitch: Switch = findViewById(R.id.normalSwitch)
        normalSwitch.setOnCheckedChangeListener {_, isChecked ->

            if (isChecked) {
                Toast.makeText(this, "Switch activado", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Switch desactivado", Toast.LENGTH_SHORT).show()
            }

        }

        var floatingActionButton = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        floatingActionButton.setOnClickListener {
            Toast.makeText(this, "Floating button pulsado", Toast.LENGTH_SHORT).show()
        }

    }

    fun onRadioButtonClicked(view: View) {

        if (view is RadioButton) {

            var checked = view.isChecked

            when (view.id) {

                R.id.playaRadioButton -> {

                    if (checked) {
                        Toast.makeText(this, "Vamos a la playa", Toast.LENGTH_SHORT).show()
                    }

                }

                R.id.montañaRadioButton -> {

                    if (checked) {
                        Toast.makeText(this, "Vamos a la montaña", Toast.LENGTH_SHORT).show()
                    }

                }

            }

        }

    }

    fun onCheckBoxClicked(view: View) {

        if (view is CheckBox) {

            var checked = view.isChecked

            when (view.getId()) {

                R.id.seguroCheckBox -> {

                    if (checked) {
                        Toast.makeText(this, "Seguro contratado", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Seguro anulado", Toast.LENGTH_SHORT).show()
                    }

                }

                R.id.cancelableCheckBox -> {

                    if (checked) {
                        Toast.makeText(this, "La reserva podra ser cancelada", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "La reserva NO podra ser cancelada", Toast.LENGTH_SHORT).show()
                    }

                }

            }

        }

    }

}