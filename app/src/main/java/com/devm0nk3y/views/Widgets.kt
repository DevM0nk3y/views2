package com.devm0nk3y.views

import android.content.Context
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.CalendarView
import android.widget.MediaController
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import android.widget.VideoView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.Calendar

class Widgets : AppCompatActivity() {

    private lateinit var activityContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widgets)

        activityContext = this

        var webView = findViewById<WebView>(R.id.webView)
        var webSettings: WebSettings = webView.getSettings()
        webSettings.javaScriptEnabled = true
        webView.setWebViewClient(WebViewClient())

        webView.loadUrl("http://www.google.com")

        var localVideoView = findViewById<VideoView>(R.id.localVideoView)
        var localMediaController  = MediaController(this)
        var path = "android.resource://" + packageName + "/" + R.raw.overwatch_dragons

        localMediaController.setAnchorView(localVideoView)
        localVideoView.setVideoURI(Uri.parse(path))
        localVideoView.setMediaController(localMediaController)

        var webVideoView = findViewById<VideoView>(R.id.webVideoView)
        var mediaController  = MediaController(this)

        mediaController.setAnchorView(webVideoView)
        webVideoView.setVideoPath("https://jotajotavm.com/img/video.mp4")
        webVideoView.setMediaController(mediaController)

        var calendarView = findViewById<CalendarView>(R.id.calendarView)
        var fechaTextView = findViewById<TextView>(R.id.fechaTextView)

        calendarView.setOnDateChangeListener { calendarView, year, month, day ->

            var date = "$day/${month+1}/$year"

            fechaTextView.text = "Fecha seleccionada: $date"

        }

        var calendar = Calendar.getInstance()
        calendar.set(2026, 5-1, 8)
        calendarView.date = calendar.timeInMillis

        var fecha = calendarView.date
        var dia = calendarView.firstDayOfWeek

        calendarView.firstDayOfWeek = dia+1 % 7

        var determinateProgressBar = findViewById<ProgressBar>(R.id.determinateProgressBar)
        var secundariaProgressBar = findViewById<ProgressBar>(R.id.secundariaProgressBar)

        determinateProgressBar.max = 100
        determinateProgressBar.progress = 0

        secundariaProgressBar.progress = 0
        secundariaProgressBar.secondaryProgress = 0

        var seekBar = findViewById<SeekBar>(R.id.seekBar)
        seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

                if (fromUser) {
                    Toast.makeText(activityContext, "Tu lo cambiaste", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        GlobalScope.launch {

            progressManager(determinateProgressBar)
            progressManager(secundariaProgressBar)
            seekBarManager(seekBar)

        }

        var ratingBar = findViewById<RatingBar>(R.id.ratingBar)
        var ratingTextView = findViewById<TextView>(R.id.ratingTextView)

        ratingBar.rating = 2.5f
        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, _ ->

            ratingTextView.text = "${rating}/${ratingBar.numStars}"

        }

    }

    private fun progressManager(progressBar: ProgressBar) {

        while (progressBar.progress < progressBar.max) {

            Thread.sleep(100L)
            //progressBar.progress += 5
            progressBar.incrementProgressBy(5)

            if (progressBar.id == R.id.secundariaProgressBar) {
                progressBar.incrementSecondaryProgressBy(10)
            }

        }

    }

    private fun seekBarManager(sb: SeekBar) {

        while (true) {

            Thread.sleep(100L)
            sb.incrementProgressBy(5)

        }

    }

}