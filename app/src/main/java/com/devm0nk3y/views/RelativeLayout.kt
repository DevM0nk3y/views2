package com.devm0nk3y.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class RelativeLayout : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_layout)
    }
}