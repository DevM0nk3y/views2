package com.devm0nk3y.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView

class SearchView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_view)

        val users = arrayOf("Alberto", "Alvaro", "Ana", "Amparo", "Bartolo", "Bernardo", "Carla", "Carlos", "Carolina")
        val adapterUsers: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_list_item_1, users)

        val searchView = findViewById<SearchView>(R.id.searchView)
        val usersListView = findViewById<ListView>(R.id.usersListView)

        usersListView.adapter = adapterUsers

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {

                searchView.clearFocus()
                if (users.contains(query)) {
                    adapterUsers.filter.filter(query)
                }
                return false

            }

            override fun onQueryTextChange(query: String?): Boolean {
                adapterUsers.filter.filter(query)
                return false
            }

        })

    }

}