package com.devm0nk3y.views

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.MultiAutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.addTextChangedListener

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var ejemploTextView = findViewById<TextView>(R.id.ejemploTextView)
        ejemploTextView.text = "Texto cambiado desde codigo"
        ejemploTextView.setTextColor(Color.RED)

        ejemploTextView.setOnClickListener {
            Toast.makeText(this, "TextView Clicked", Toast.LENGTH_LONG).show()
            ejemploTextView.setTextColor(Color.GREEN)
        }

        var ejemploEditText = findViewById<EditText>(R.id.ejemploEditText)
        ejemploEditText.addTextChangedListener {

            if (ejemploEditText.text.length == 0) {
                ejemploEditText.setError("Campo vacio")
            }

        }

        var autoCompleteTextView = findViewById<AutoCompleteTextView>(R.id.autoCompleteTextView)
        var countries: Array<String> = resources.getStringArray(R.array.countries_array)

        var adapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, countries)

        autoCompleteTextView.setAdapter(adapter)

        var multiAutoCompleteTextView = findViewById<MultiAutoCompleteTextView>(R.id.multiAutoCompleteTextView)
        multiAutoCompleteTextView.setAdapter(adapter)
        multiAutoCompleteTextView.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())

    }

}